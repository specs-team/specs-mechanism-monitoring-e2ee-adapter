# E2EE Monitoring Adapter

This component, part of the E2EE Security Mechanism, monitors databases on both storage sites (i.e., checks if backups and restorations of backup are performed successfully, monitors availability of both servers, and availability of both DBs).

## Usage

The E2EE Monitoring Adapter is to be used with the following components:
- [E2EE Client](https://bitbucket.org/specs-team/specs-mechanism-enforcement-e2ee-client)
- [E2EE Server](https://bitbucket.org/specs-team/specs-mechanism-enforcement-e2ee-server)

# NOTICE #

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

* http://www.specs-project.eu/
* http://www.xlab.si/

*Developers:*

    Miha Stopar, miha.stopar@xlab.si

*Copyright:*

```
Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si
    
E2EE Server is free software: you can redistribute it and/or modify it
under the terms of the Affero GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Crypton Server is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
License for more details.

You should have received a copy of the Affero GNU General Public License
along with Crypton Server.  If not, see <http://www.gnu.org/licenses/>.
```
